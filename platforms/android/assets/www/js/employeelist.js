var employees;

$('#employeeListPage').bind('pageinit', function(event) {
	getEmployeeList();
});

function getEmployeeList() {
	$('#employeeList li').remove();		
	$('#employeeList').append('<li> <h4>Grade 1</h4></li>');
	$('#employeeList').append('<li> <h4>Grade 2</h4></li>');
	$('#employeeList').append('<li> <h4>Grade 3</h4></li>');
	$('#employeeList').append('<li> <h4>Grade 4</h4></li>');
	$('#employeeList').append('<li> <h4>Grade 5</h4></li>');
	$('#employeeList').listview('refresh');
}